from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import auth, User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import CreateUserForm

@login_required(login_url='main:login')
def home(request):
    return render(request, 'main/home.html')

def login(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    else :
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
        
            user = auth.authenticate(request, username=username, password=password)
        
            if user is not None:
                auth.login(request,user)
                # print('kelogin')
                return redirect('main:home')
            else :
                messages.info(request, 'Username or Password is incorrect')
    return render(request, 'main/login.html')

def logout(request):
    auth.logout(request)
    return redirect('main:login')

def register(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                # print('berhasil')
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

    context = {'form' : form}
    return render(request, 'main/register.html', context)
