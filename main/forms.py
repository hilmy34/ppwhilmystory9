from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    username_attrs = {
    'type': 'text',
    'placeholder':'Username.. '
    }
    email_attrs = {
    'type': 'text',
    'placeholder':'Email..'
    }

    pass1_attrs = {
    'type': 'password',
    'placeholder':'Password..'
    }
    
    pass2_attrs = {
    'type': 'password',
    'placeholder':'Confirm Password..'
    }
    username = forms.CharField(required=True, widget=forms.TextInput(attrs=username_attrs))
    email = forms.EmailField(required=True,widget=forms.TextInput(attrs=email_attrs))
    password1 = forms.CharField(required=True,widget=forms.PasswordInput(attrs=pass1_attrs))
    password2 = forms.CharField(required=True,widget=forms.PasswordInput(attrs=pass2_attrs))
