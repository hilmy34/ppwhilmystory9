from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import login, register
from django.contrib.auth.models import auth, User

class TestStory9(TestCase):
    def test_home_no_login(self):
        response = Client().get('')
        self.assertEquals(302, response.status_code)

    def test_template_login(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'main/login.html')

    def test_template(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code,200)
        self.assertEquals(200, response.status_code)

    #view

    def test_view_login(self):
        user = User.objects.create_user(username='pepeweasik', email='hilmyakbar02@gmail.com', password='storyterakhir123')
        user.save()
        response = self.client.post('/login/',data= {
			'username': 'pepeweasik',
			'password': 'storyterakhir123'
			})
        self.assertEquals(response.status_code,302)

    def test_view_login_salah(self):
        user = User.objects.create_user(username='pepeweasik', email='hilmyakbar02@gmail.com', password='storyterakhir123')
        user.save()
        response = self.client.post('/login/',data= {
			'username': 'asikpepewe',
			'password': '123storyterakhir'
			})
        self.assertEquals(response.status_code,200)

    def test_view_login_lagi(self):
        user = User.objects.create_user(username='test', password="pass")
        self.client.login(username="test", password="pass")

        response = self.client.get('/login/')
        self.assertEquals(response.status_code,302)

    def test_view_register(self):
        response = self.client.post('/register/', data= {
			'username':'uname',
			'email':'ahay@gmail.com',
			'password':'password',
			'password1':'password'
		    })
        self.assertEquals(response.status_code, 200)

    def test_view_register2(self):
        user = User.objects.create_user(username='test', password="pass")
        self.client.login(username="test", password="pass")

        response = self.client.get('/register/')
        self.assertEquals(response.status_code,302)

    def test_view_register3(self):
        user = User.objects.create_user(username='testingaja', password="pass123456")
        response = self.client.post("/register/", data={
            "username":"testingaja",
            "password":"pass123456",
            "password1":"pass123456",
            "email":"test@mail.com"
            })
        self.assertEquals(response.status_code, 200)

    def test_view_home(self):
        user = User.objects.create_user(username='uname', password="pass")
        self.client.login(username="uname", password="pass")
        response = self.client.get('')

        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, "main/home.html")

    def test_view_logout(self):
        response = self.client.post('/logout/')
        self.assertEquals(response.status_code,302)
